# IMDB2BBCode


## Setting Up

1. Get your api key from [omdbapi](https://www.omdbapi.com/)
2. Clone this repo or download as zip
3. Install [Python 3](https://www.python.org/downloads/)

## Usage 

1. Paste your api key in api_key
2. Run the script with the IMDb ID
	
	`python imdb2bb.py [IMDb ID]`

3. BBCode will be in the imdb2bb_out.txt 