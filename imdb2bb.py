import urllib.request
import json
import sys

#Coded by A99mus
#https://github.com/Ashish0804/IMDb2BBCode



#Paste your api key here
api_key=''
base_url='http://www.omdbapi.com/?apikey='+api_key
imbd_tag=str(sys.argv[1])
imbd_url='https://www.imdb.com/title/'+imbd_tag
url=base_url+'&i='+imbd_tag
data = json.load(urllib.request.urlopen(url))

out_file=open("imbd2bb_out.txt","w")


out_template='''
[center][size=150]{}({})[/size][/center]

[center][url={}]{}[/url][/center]



[center][img]{}[/img][/center]


[color=#FF8000][b]Release Date[/b][/color]: {}

[color=#FF8000][b]Viewer Rating[/b][/color]: {}

[color=#FF8000][b]Runtime[/b][/color]: {}

[color=#FF8000][b]Genre[/b][/color]: {}

[color=#FF8000][b]Actors[/b][/color]: {}

[color=#FF8000][b]Plot[/b][/color]: {}
'''

out_text= out_template.format(data['Title'],data['Year'],imbd_url,imbd_tag,data['Poster'],data['Released'],data['Rated'],data['Runtime'],data['Genre'],data['Actors'],data['Plot'])

out_file.write(out_text)
out_file.close()

